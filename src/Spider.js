import { HEIGHT, SPIDER_SIZE, WIDTH } from './constants';

export default class Spider {
  constructor(canvas) {
    this.canvas = canvas;
  }

  x = 0;

  y = 0;

  render() {
    this.canvas.drawSquare(
      Math.max(0, this.x - SPIDER_SIZE / 2),
      Math.max(0, this.y - SPIDER_SIZE / 2),
      Math.min(WIDTH, this.y + SPIDER_SIZE / 2),
      Math.min(HEIGHT, this.y + SPIDER_SIZE / 2),
      '#000000'
    );
  }
}
