const keysDown = {};
const boundKeys = {};

window.addEventListener('keydown', event => {
  keysDown[event.keyCode] = true;
});

window.addEventListener('keyup', event => {
  delete keysDown[event.keyCode];
});

export const registerKeyBind = (keyCode, action) => {
  boundKeys[keyCode] = action;
};

export const checkKeys = delta => {
  Object.keys(keysDown)
    .map(value => Number(value))
    .forEach(value => {
      const action = boundKeys[value];
      if (action) {
        action(delta);
      }
    });
};
