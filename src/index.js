import Canvas from './canvas2d';
import { startGameTick } from './gametick';
import { checkKeys } from './keys';
import { addStrand, getStrands } from './web';
import Spider from './Spider';
import { HEIGHT, WIDTH } from './constants';

const canvas = new Canvas('canvas', WIDTH, HEIGHT);

const WHITE = '#FFFFFF';

addStrand(0, 0, WIDTH, 0);
addStrand(0, 0, 0, HEIGHT);
addStrand(WIDTH, 0, WIDTH, HEIGHT);
addStrand(0, HEIGHT, WIDTH, HEIGHT);

// Web demo start
addStrand(0, 0, WIDTH, HEIGHT);
addStrand(WIDTH, 0, 0, HEIGHT);
addStrand(WIDTH / 2, 0, WIDTH / 2, HEIGHT);
addStrand(0, HEIGHT / 2, WIDTH, HEIGHT / 2);

addStrand(WIDTH / 8, HEIGHT / 8, WIDTH / 2, 0);
addStrand(WIDTH - WIDTH / 8, HEIGHT / 8, WIDTH / 2, 0);
addStrand(WIDTH / 8, HEIGHT - HEIGHT / 8, WIDTH / 2, HEIGHT);
addStrand(WIDTH - WIDTH / 8, HEIGHT - HEIGHT / 8, WIDTH / 2, HEIGHT);
addStrand(0, HEIGHT / 2, WIDTH / 8, HEIGHT - HEIGHT / 8);
addStrand(0, HEIGHT / 2, WIDTH / 8, HEIGHT / 8);
addStrand(WIDTH, HEIGHT / 2, WIDTH - WIDTH / 8, HEIGHT - HEIGHT / 8);
addStrand(WIDTH, HEIGHT / 2, WIDTH - WIDTH / 8, HEIGHT / 8);

addStrand(WIDTH / 4, HEIGHT / 4, WIDTH / 2, HEIGHT / 6);
addStrand(WIDTH - WIDTH / 4, HEIGHT / 4, WIDTH / 2, HEIGHT / 6);
addStrand(WIDTH / 4, HEIGHT - HEIGHT / 4, WIDTH / 2, HEIGHT - HEIGHT / 6);
addStrand(WIDTH - WIDTH / 4, HEIGHT - HEIGHT / 4, WIDTH / 2, HEIGHT - HEIGHT / 6);
addStrand(WIDTH / 6, HEIGHT / 2, WIDTH / 4, HEIGHT - HEIGHT / 4);
addStrand(WIDTH / 6, HEIGHT / 2, WIDTH / 4, HEIGHT / 4);
addStrand(WIDTH - WIDTH / 6, HEIGHT / 2, WIDTH - WIDTH / 4, HEIGHT - HEIGHT / 4);
addStrand(WIDTH - WIDTH / 6, HEIGHT / 2, WIDTH - WIDTH / 4, HEIGHT / 4);
// Web demo end

const spider = new Spider(canvas);

// const moveSpider = delta => {
//
// }

startGameTick({
  updateFunc: delta => {
    checkKeys(delta);
  },
  renderFunc: () => {
    canvas.drawBackground('#AACCFF');
    getStrands().forEach(v => {
      canvas.drawLine(v[0].x, v[0].y, v[1].x, v[1].y, WHITE);
    });
    spider.render();
  },
  tickRate: 15,
});
