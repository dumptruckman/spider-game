export const startGameTick = ({ updateFunc, renderFunc, tickRate }) => {
  let time = Date.now();
  setInterval(() => {
    const now = Date.now();
    const delta = now - (time || now);
    time = now;

    updateFunc(delta);
    renderFunc(delta);
  }, tickRate);
};

export const stopGameTick = () => {
  clearInterval();
};
