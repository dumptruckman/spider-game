const subscribers = {};

export const subscribe = (event, callback) => {
  subscribers[event] = subscribers[event] || [];
  subscribers[event].push(callback);
};

export const publish = event => {
  if (subscribers && subscribers[event]) {
    const subs = subscribers[event];
    const args = Array.prototype.slice.call(arguments, 1);
    for (let n = 0, max = subs.length; n < max; n += 1) {
      subs[n].apply(this, args);
    }
  }
};
