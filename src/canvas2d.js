export default class Canvas {
  constructor(elementId, width, height) {
    this.canvas = document.getElementById(elementId);
    this.canvas.width = width;
    this.canvas.height = height;
    this.context = this.canvas.getContext('2d');
  }

  drawBackground = style => {
    this.context.fillStyle = style;
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
  };

  drawLine = (x1, y1, x2, y2, style) => {
    this.context.beginPath();
    this.context.moveTo(x1, y1);
    this.context.lineTo(x2, y2);
    this.context.strokeStyle = style;
    this.context.stroke();
    this.context.closePath();
  };

  drawSquare = (x1, y1, x2, y2, style) => {
    this.context.fillStyle = style;
    this.context.fillRect(x1, y1, x2, y2);
  };

  getMouse = () => {

  };
}
