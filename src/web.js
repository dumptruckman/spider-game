class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}

const vertices = [];
const edges = [];

// Creates a strand from point (a, b) to point (x, y)
export const addStrand = (a, b, x, y) => {
  let i1 = vertices.findIndex(v => v.x === a && v.y === b);
  let i2 = vertices.findIndex(v => v.x === x && v.y === y);

  if (i1 < 0) {
    i1 = vertices.push(new Point(a, b)) - 1;
    edges.forEach(v => v.push(0));
    edges.push(Array(vertices.length).fill(0));
  }
  if (i2 < 0) {
    i2 = vertices.push(new Point(x, y)) - 1;
    edges.forEach(v => v.push(0));
    edges.push(Array(vertices.length).fill(0));
  }

  edges[i1][i2] = 1;
  edges[i2][i1] = 1;
};

// export const logGraph = () => {
//   console.log('vertices: ', vertices);
//   console.log('edges:');
//   edges.forEach(v => {
//     console.log(v.join(' '));
//   });
// };

export const getStrands = () => {
  const result = [];
  edges.forEach((row, i1) => {
    row.forEach((v, i2) => {
      if (i2 > i1 && v > 0) {
        result.push([vertices[i1], vertices[i2]]);
      }
    });
  });
  return result;
};
